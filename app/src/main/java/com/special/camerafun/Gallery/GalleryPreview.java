package com.special.camerafun.Gallery;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.special.camerafun.R;

import java.io.File;

public class GalleryPreview extends AppCompatActivity {


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gallery_select_layout);
		String picPath = getIntent().getStringExtra("picPath");
		ImageView imageView = (ImageView) findViewById(R.id.gallery_select);

		File pictureFile = new File(picPath);
		Glide.with(this)
				.load(pictureFile)
				.into(imageView);
	}

	public void closeGallery(View view) {
		GalleryPreview.this.finish();
	}
}
