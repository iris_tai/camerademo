package com.special.camerafun.Gallery;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.special.camerafun.DgCamActivity;
import com.special.camerafun.R;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by iristai on 2017/10/16.
 */

public class GalleryListActivity extends AppCompatActivity {


    private TextView mNoPictureTextView;
    GalleryGridAdapter mGalleryGridAdapter;
    private ArrayList<String> itemList = new ArrayList<String>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_list_layout);

        mNoPictureTextView = (TextView) findViewById(R.id.no_picture_text);
        GridView recyclerView = (GridView) findViewById(R.id.recycleview);

        String ExternalStorageDirectoryPath = Environment
                .getExternalStorageDirectory()
                .getAbsolutePath();

        String targetPath = ExternalStorageDirectoryPath + DgCamActivity.CAMERA_DIR;

        File targetDirector = new File(targetPath);
        File[] files = targetDirector.listFiles();
//        mNoPictureTextView.setVisibility(View.VISIBLE);
        if (files.length == 0) {
            mNoPictureTextView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            mNoPictureTextView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);


            for (int i = files.length ; i > 0 ; i-- ){
                itemList.add(files[i-1].getAbsolutePath());
            }


            mGalleryGridAdapter = new GalleryGridAdapter(this , itemList , new GalleryGridAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(String imgPath) {
                    Log.d("IRIS","click path =" + imgPath);
                    Intent intent = new Intent(GalleryListActivity.this, GalleryPreview.class);
                    intent.putExtra("picPath", imgPath);
                    startActivity(intent);
                }
            });

            recyclerView.setAdapter(mGalleryGridAdapter);
        }


    }

    public void closeGallery(View view) {
        GalleryListActivity.this.finish();
    }

}
