package com.special.camerafun.Gallery;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.special.camerafun.R;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by iristai on 2017/10/18.
 */

public class GalleryGridAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;
    ArrayList<String> mItemList;
    private Context mContext;
    private OnItemClickListener mClickListener;

    public interface OnItemClickListener {
        void onItemClick(String imgPath);
    }

    public GalleryGridAdapter(Context context, ArrayList<String> itemList, OnItemClickListener clickListener)
    {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mItemList = itemList;
        mContext = context;
        this.mClickListener = clickListener;
    }

    @Override
    public int getCount()
    {
        //取得 GridView 列表 Item 的數量
        return mItemList.size();
    }

    @Override
    public Object getItem(int position)
    {
        //取得 GridView列表於 position 位置上的 Item
        return position;
    }

    @Override
    public long getItemId(int position)
    {
        //取得 GridView 列表於 position 位置上的 Item 的 ID
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        View v = mLayoutInflater.inflate(R.layout.grid_item, parent, false);

        ImageView imgView = (ImageView) v.findViewById(R.id.grid_view_item);
        File pictureFile = new File(mItemList.get(position));
        Glide.with(mContext)
                .load(pictureFile)
                .into(imgView);
        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onItemClick(mItemList.get(position));
            }
        });

        return v;
    }

}
