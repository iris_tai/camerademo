package com.special.camerafun;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class CameraResult extends Activity {

    private String picPath;
    private ImageView mRetakeButton;
    private ImageView mSaveBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_layout);
        picPath = getIntent().getStringExtra("picPath");
        ImageView imageView = (ImageView) findViewById(R.id.iv_camera_result);

        mRetakeButton = (ImageView) findViewById(R.id.ibRetake);
        mSaveBtn = (ImageView) findViewById(R.id.ibUse);

        mRetakeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showGiveUpDialog();
            }
        });
        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickToSave();
                Toast.makeText(getApplicationContext(), "儲存成功", Toast.LENGTH_LONG).show();
                CameraResult.this.finish();
            }
        });
        imageView.setImageBitmap(getBitmapFromPath(picPath));
    }


    private Bitmap getBitmapFromPath(String path) {
        FileInputStream fis = null;
        Bitmap bitmap = null;
        try {
            fis = new FileInputStream(path);
            bitmap = BitmapFactory.decodeStream(fis);
//            Matrix matrix = new Matrix();
//            matrix.setRotate(90);
//            bitmap = Bitmap.createBitmap(bitmap, 0, 0,
//                    bitmap.getWidth(), bitmap.getHeight(),
//                    matrix, true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bitmap;
    }

    private void removePhotos() {
        File discardedPhoto = new File(picPath);
        discardedPhoto.delete();
    }

    public void clickToSave() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(picPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    private void showGiveUpDialog() {
        new MaterialDialog.Builder(this)
                .title(R.string.exist_giveup_photos_title)
                .titleColor(getResources().getColor(R.color.black))
                .inputType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT)
                .content(R.string.exist_confirm_content)
                .contentColor(getResources().getColor(R.color.black))
                .negativeText(R.string.text_cancel)
                .positiveColorRes(R.color.dialog_positive_button_color)
                .positiveText(R.string.text_sure)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        removePhotos();
                        CameraResult.this.finish();
                    }
                }).show();
    }
    @Override
    public void onBackPressed() {
        showGiveUpDialog();
    }
}
