package com.special.camerafun;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.special.camerafun.home.MainActivity;

/**
 * Created by iristai on 2017/10/14.
 */

public class LaunchActivity extends Activity {

    private int count_number = 0;
    private static final int COUNT_DOWN = 1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lay_launch);

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (count_number < COUNT_DOWN) {
                    count_number++;
                    handler.postDelayed(this, 1000);
                } else {
                    Intent intent = new Intent(LaunchActivity.this, MainActivity.class);
                    startActivity(intent);
                    LaunchActivity.this.finish();
                }

            }
        };
        handler.post(runnable);

    }
}
