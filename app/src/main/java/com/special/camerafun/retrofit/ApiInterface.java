package com.special.camerafun.retrofit;

import com.special.camerafun.model.BannerList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by iristai on 2017/11/9.
 */

public interface ApiInterface {

    @GET("/API/Ban/getList.html")
    Call<BannerList> bannerlist();
}
