package com.special.camerafun.model;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

/**
 * Created by iristai on 2017/11/12.
 */
@Parcel
public class DataList {

    String banImg;
    String banUrl;
    String banTitle;

    @ParcelConstructor
    public DataList(String banImg, String banUrl, String banTitle) {
        this.banImg = banImg;
        this.banUrl = banUrl;
        this.banTitle = banTitle;
    }

    public String getBanImg() {
        return banImg;
    }

    public void setBanImg(String banImg) {
        this.banImg = banImg;
    }

    public String getBanUrl() {
        return banUrl;
    }

    public void setBanUrl(String banUrl) {
        this.banUrl = banUrl;
    }

    public String getBanTitle() {
        return banTitle;
    }

    public void setBanTitle(String banTitle) {
        this.banTitle = banTitle;
    }
}
