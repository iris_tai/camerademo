package com.special.camerafun.model;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import java.util.List;

/**
 * Created by iristai on 2017/11/12.
 */
@Parcel
public class BannerList {

    boolean success;
    String resultMessage;
    String resultCode;
    List<DataList> data;

    @ParcelConstructor
    public BannerList(boolean success, String resultMessage, String resultCode, List<DataList> data) {
        this.success = success;
        this.resultMessage = resultMessage;
        this.resultCode = resultCode;
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public List<DataList> getData() {
        return data;
    }

    public void setData(List<DataList> data) {
        this.data = data;
    }
}
