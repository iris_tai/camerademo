package com.special.camerafun;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.special.camerafun.Gallery.GalleryListActivity;
import com.special.camerafun.Multitouch.PhotoSortrView;
import com.special.camerafun.home.MainActivity;
import com.special.camerafun.webview.WebViewActivity;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class DgCamActivity extends AppCompatActivity implements SensorEventListener {
	private Camera mCamera;
	private CameraPreview mPreview;
	private Camera.Parameters mParams;
	private Camera.Size optimalSize;
	private SensorManager sensorManager = null;
	private int orientation;
	private ExifInterface exif;
	private int deviceHeight,deviceWeight;

	private PhotoSortrView photoSorter;
	private ImageView mCapture, mSetting, mCheckPhoto, mFaceImages , mRotateCamera;
	private File sdRoot;
	private String fileName;
	private FrameLayout mFrameLayout;
	private RelativeLayout mToolsLayout;
	private Bitmap cameraBitmap,imageBitmap, allBitmap;
	private static final String TAG = "DgCamActivity";
	private int degrees = 0;
	private Paint mPaint;
	private OrientationEventListener orientationEventListener;
	private static final int CAMERA_BACK = 0;
	private static final int CAMERA_FRONT = 1;
	private int cameraPosition = CAMERA_BACK;  //0是後置鏡頭，1是前置鏡頭

	// Remember some things for zooming
    public static final String SELECT_IMG_URL = "select_img_url";
    private String mPassImg = "";
    private Drawable mSelectDrawable;

    private ImageView mInVisibleImage;

	public static final String CAMERA_DIR = "/DCIM/Camera/CameraFun/";

	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_camera);

        mPassImg = getIntent().getExtras().getString(SELECT_IMG_URL,"");


        // Setting all the path for the image
		sdRoot = Environment.getExternalStorageDirectory();

        mInVisibleImage = (ImageView) findViewById(R.id.add_display_image);
		// Getting all the needed elements from the layout
		mToolsLayout = (RelativeLayout) findViewById(R.id.tools_layout);
		mCapture = (ImageView) findViewById(R.id.capture_imageView);
		mRotateCamera = (ImageView) findViewById(R.id.rotate_img);
//		mSetting = (ImageView) findViewById(R.id.setting_img);
		mFaceImages = (ImageView) findViewById(R.id.face_img);
		mCheckPhoto = (ImageView) findViewById(R.id.photos_img);
		mFrameLayout = (FrameLayout) findViewById(R.id.camera_preview);
		photoSorter = (PhotoSortrView) findViewById(R.id.photosortr);

		mFaceImages.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
                clickIcons();
			}
		});

		mCheckPhoto.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(DgCamActivity.this, GalleryListActivity.class);
				startActivity(intent);
			}
		});

        initSelectImage();


		// Getting the sensor service.
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

		Display display = getWindowManager().getDefaultDisplay();
		String displayName = display.getName();  // minSdkVersion=17+
		Log.i(TAG, "displayName  = " + displayName);

// display size in pixels
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;
		Log.i(TAG, "width        = " + width);
		Log.i(TAG, "height       = " + height);
		deviceHeight = height;
		deviceWeight = width;
		// Add a listener to the Capture button
		mCapture.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				mCamera.takePicture(null, null, mPicture);
			}
		});
		mRotateCamera.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				switchCamera();
			}
		});

	}

    private void initSelectImage() {
        if (mPassImg != null && !mPassImg.equals("")) {
            mInVisibleImage.destroyDrawingCache();
            Glide.with(this).load(mPassImg).into(mInVisibleImage);

            Handler handler = new Handler();
            handler.postDelayed(new Runnable(){

                @Override
                public void run() {
                    mSelectDrawable = new BitmapDrawable(getResources(), convertViewToBitmap(mInVisibleImage));
                    addNewImage(mSelectDrawable);

                }}, 2000);

        }

    }

    public void clickIcons() {
        Intent intent = new Intent(DgCamActivity.this, WebViewActivity.class);
        intent.putExtra(WebViewActivity.PARAM_URL, MainActivity.ICONS_URL);
		intent.putExtra(WebViewActivity.PARAM_WEBVIEW_TITLE, getString(R.string.icon_folder_title));
		startActivity(intent);
        DgCamActivity.this.finish();
    }

    public static Bitmap convertViewToBitmap(View view){
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache();
        Bitmap bitmap = view.getDrawingCache();

        return bitmap;
    }

	@Override
	protected void onResume() {
		super.onResume();

		// Test if there is a camera on the device and if the SD card is
		// mounted.
		if (!checkCameraHardware(this)) {
			Intent i = new Intent(this, NoCamera.class);
			startActivity(i);
			finish();
		} else if (!checkSDCard()) {
			Intent i = new Intent(this, NoSDCard.class);
			startActivity(i);
			finish();
		}
		photoSorter.destroyDrawingCache();
		DgCamActivityPermissionsDispatcher.createCameraPermitionWithCheck(this);

		// Register this class as a listener for the accelerometer sensor
		sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
	}

    private void addNewImage(Drawable drawable) {

        photoSorter.destroyDrawingCache();
        photoSorter.removeAllImages();
        photoSorter.addImages(DgCamActivity.this, drawable);
    }

	@Override
	protected void onPause() {
		super.onPause();
		// release the camera immediately on pause event
		releaseCamera();
		photoSorter.destroyDrawingCache();
	}

	private void releaseCamera() {
		if (mCamera != null) {
			mCamera.stopPreview();
			mCamera.release(); // release the camera for other applications
			mCamera = null;
			mPreview.surfaceDestroyed(mPreview.getHolder());
			mPreview.getHolder().removeCallback(mPreview);
			mPreview.destroyDrawingCache();
			mFrameLayout.removeView(mPreview);
		}
	}

	/** Check if this device has a camera */
	private boolean checkCameraHardware(Context context) {
		if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
			// this device has a camera
			return true;
		} else {
			// no camera on this device
			return false;
		}
	}

	private boolean checkSDCard() {
		boolean state = false;

		String sd = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(sd)) {
			state = true;
		}

		return state;
	}

	/**
	 * A safe way to get an instance of the Camera object.
	 */
	public static Camera getCameraInstance(int position) {
		Camera c = null;
		try {
			// attempt to get a Camera instance
			c = Camera.open(position);
		} catch (Exception e) {
			// Camera is not available (in use or does not exist)
		}

		// returns null if camera is unavailable
		return c;
	}

	private PictureCallback mPicture = new PictureCallback() {

		public void onPictureTaken(byte[] data, Camera camera) {

			// Replacing the button after a photho was taken.

			// File name of the image that we just took.
			fileName = "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()).toString() + ".jpg";

			// Creating the directory where to save the image. Sadly in older
			// version of Android we can not get the Media catalog name
			File mkDir = new File(sdRoot, CAMERA_DIR);
			mkDir.mkdirs();

			// Main file where to save the data that we recive from the camera
			File pictureFile = new File(sdRoot, CAMERA_DIR + fileName);

			try {
				FileOutputStream purge = new FileOutputStream(pictureFile);
				purge.write(data);
				purge.close();
			} catch (FileNotFoundException e) {
				Log.d("DG_DEBUG", "File not found: " + e.getMessage());
			} catch (IOException e) {
				Log.d("DG_DEBUG", "Error accessing file: " + e.getMessage());
			}
			Bitmap handleCameraBitmap;
			Matrix handleMatrix = new Matrix();
			cameraBitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
			Log.d(TAG, "cameraBitmap width=" + cameraBitmap.getWidth());
			Log.d(TAG, "cameraBitmap heigh=" + cameraBitmap.getHeight());
			if (cameraPosition == CAMERA_FRONT) {
				handleMatrix.postScale(-1, 1);
			}
			handleCameraBitmap = Bitmap.createBitmap(cameraBitmap, 0, 0,
					cameraBitmap.getWidth(), cameraBitmap.getHeight(), handleMatrix, true);
			//#################
			Log.d(TAG,"mSelectImg width="+ photoSorter.getWidth());
			Log.d(TAG,"mSelectImg heigh="+ photoSorter.getHeight());
			if (photoSorter != null && photoSorter.getHeight() != 0) {
				photoSorter.buildDrawingCache();

				imageBitmap = photoSorter.getDrawingCache();

				float scaleSize = (float) optimalSize.height / photoSorter.getWidth();
				Log.d(TAG, "scaleSize=" + scaleSize);
				Matrix selectMatrix = new Matrix();
				selectMatrix.postScale(scaleSize, scaleSize);
				imageBitmap = Bitmap.createBitmap(imageBitmap, 0, 0, photoSorter.getWidth(), photoSorter.getHeight(), selectMatrix, true);

				allBitmap = overlay(RotateBitmap(handleCameraBitmap, 90), imageBitmap);

				allBitmap = RotateBitmap(allBitmap, - degrees);

			} else {
				allBitmap = RotateBitmap(handleCameraBitmap, 90);
			}
			try {
				FileOutputStream fos = new FileOutputStream(pictureFile);
				allBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
				fos.flush();
				fos.close();
				Intent intent = new Intent(DgCamActivity.this, CameraResult.class);
				intent.putExtra("picPath", pictureFile.getAbsolutePath());
				startActivity(intent);

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	};

	public static Bitmap RotateBitmap(Bitmap source, float angle)
	{
		Matrix matrix = new Matrix();
		matrix.postRotate(angle);
		return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}

	private Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
		Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
		Canvas canvas = new Canvas(bmOverlay);
		canvas.drawBitmap(bmp1, new Matrix(), null);
		canvas.drawBitmap(bmp2, 0, bmp1.getHeight() - bmp2.getHeight(), mPaint);
		return bmOverlay;
	}

	/**
	 * Putting in place a listener so we can get the sensor data only when
	 * something changes.
	 */
	public void onSensorChanged(SensorEvent event) {
		synchronized (this) {
			if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
				RotateAnimation animation = null;
				if (event.values[0] < 4 && event.values[0] > -4) {
					if (event.values[1] > 0 && orientation != ExifInterface.ORIENTATION_ROTATE_90) {
						// UP
						animation = getRotateAnimation(0);
						degrees = 0;
					} else if (event.values[1] < 0 && orientation != ExifInterface.ORIENTATION_ROTATE_270) {
						// UP SIDE DOWN
						animation = getRotateAnimation(180);
						degrees = 180;
					}
				} else if (event.values[1] < 4 && event.values[1] > -4) {
					if (event.values[0] > 0 && orientation != ExifInterface.ORIENTATION_NORMAL) {
						// LEFT
						animation = getRotateAnimation(90);
						Log.d(TAG,"degree = 90");
						degrees = 90;
					} else if (event.values[0] < 0 && orientation != ExifInterface.ORIENTATION_ROTATE_180) {
						// RIGHT
						animation = getRotateAnimation(270);
						degrees = 270;
					}
				}
				if (animation != null) {
					mCapture.startAnimation(animation);
					mRotateCamera.startAnimation(animation);
					mCheckPhoto.startAnimation(animation);
					mFaceImages.startAnimation(animation);
				}
			}

		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int i) {

	}

	/**
	 * Calculating the degrees needed to rotate the image imposed on the button
	 * so it is always facing the user in the right direction
	 * 
	 * @param toDegrees
	 * @return
	 */
	private RotateAnimation getRotateAnimation(float toDegrees) {
		float compensation = 0;

		if (Math.abs(degrees - toDegrees) > 180) {
			compensation = 360;
		}

		// When the device is being held on the left side (default position for
		// a camera) we need to add, not subtract from the toDegrees.
		if (toDegrees == 0) {
			compensation = -compensation;
		}

		// Creating the animation and the RELATIVE_TO_SELF means that he image
		// will rotate on it center instead of a corner.
		RotateAnimation animation = new RotateAnimation(degrees, toDegrees - compensation, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

		// Adding the time needed to rotate the image
		animation.setDuration(250);

		// Set the animation to stop after reaching the desired position. With
		// out this it would return to the original state.
		animation.setFillAfter(true);

		return animation;
	}

	@NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
	void createCameraPermition() {
		// Create an instance of Camera
		createCamera(0);
	}

	private void createCamera(int position) {
		mCamera = getCameraInstance(position);

		// Setting the right parameters in the camera
		Camera.Parameters params = mCamera.getParameters();
		List<Camera.Size> mSupportedPreviewSizes = params.getSupportedPreviewSizes();
		List<Camera.Size> mSupportedPictureSizes = params.getSupportedPictureSizes();
		optimalSize = CameraHelper.getOptimalPictureSize(mSupportedPictureSizes,
				mSupportedPreviewSizes, deviceHeight, deviceWeight);

		Log.d(TAG,"optimalSize  W="+ optimalSize.width+",H="+ optimalSize.height);

		params.setPictureSize(optimalSize.width, optimalSize.height);
		params.setPreviewSize(optimalSize.width, optimalSize.height);
		params.setPictureFormat(ImageFormat.JPEG);

		params.setJpegQuality(100);
		mCamera.setParameters(params);

		mCamera.setDisplayOrientation(90);
		// Create our Preview view and set it as the content of our activity.
		mPreview = new CameraPreview(this, mCamera, getWindowManager().getDefaultDisplay());

		mFrameLayout.addView(mPreview, 0);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		DgCamActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
	}

	@Override
	public void onBackPressed() {
        this.finish();
	}

	private void switchCamera() {
		//切换前后摄像头
		int cameraCount = 0;
		Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
		cameraCount = Camera.getNumberOfCameras();//得到摄像头的个数
		for (int i = 0; i < cameraCount; i++) {
			Camera.getCameraInfo(i, cameraInfo);//得到每一个摄像头的信息
			if (cameraCount >= 2 && cameraPosition == CAMERA_BACK) {
				//现在是后置，变更为前置
				if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
					openCamera(CAMERA_FRONT);
					break;
				}
			} else {
				//现在是前置， 变更为后置
				if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
					openCamera(CAMERA_BACK);
					break;
				}
			}

		}
	}

	private void openCamera(int position) {
		releaseCamera();
		createCamera(position);
		cameraPosition = position;
	}

}