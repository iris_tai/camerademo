package com.special.camerafun.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.special.camerafun.DgCamActivity;
import com.special.camerafun.Gallery.GalleryListActivity;
import com.special.camerafun.R;
import com.special.camerafun.model.BannerList;
import com.special.camerafun.model.DataList;
import com.special.camerafun.retrofit.ApiClient;
import com.special.camerafun.retrofit.ApiInterface;
import com.special.camerafun.webview.WebViewActivity;
import com.stx.xhb.xbanner.XBanner;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by iristai on 2017/10/28.
 */

public class MainActivity extends AppCompatActivity implements XBanner.XBannerAdapter{

    private XBanner mBannerNet;
    private List<String> imgesUrl;
    private List<String> data;
    private BannerList mBannerList;
    private ApiInterface mApiInterface;
    public static final String ICONS_URL = "http://mem.tsheen.com/Main/Theme.html";
    public static final String SETTINGS_URL = "http://mem.tsheen.com/Main/Setup.html";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainpage);

        callApi();
        initViews();

    }

    private void initViews() {
        mBannerNet = (XBanner) findViewById(R.id.banner_1);
    }

    private void callApi() {
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        /**
         GET List Resources
         **/
        Call call = mApiInterface.bannerlist();
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {


                Log.d("TAG",response.code()+"");
                mBannerList = (BannerList) response.body();
                initNetBanner();
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                call.cancel();
            }
        });

    }

    private void initNetBanner() {

        imgesUrl = new ArrayList<>();
        for (DataList dataList : mBannerList.getData()) {
            imgesUrl.add(dataList.getBanImg());
        }

        mBannerNet.setData(imgesUrl, null);

        mBannerNet.setOnItemClickListener(new XBanner.OnItemClickListener() {
            @Override
            public void onItemClick(XBanner xBanner, int i) {
                Log.d("MainActivity","position = " + i);
                if (mBannerList.getData().get(i).getBanUrl() == null || mBannerList.getData().get(i).getBanUrl().equals("")) return;
                Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
                intent.putExtra(WebViewActivity.PARAM_URL, mBannerList.getData().get(i).getBanUrl());
                intent.putExtra(WebViewActivity.PARAM_WEBVIEW_TITLE, mBannerList.getData().get(i).getBanTitle());
                startActivity(intent);
            }
        });
        mBannerNet.setmAdapter(this);
    }

    @Override
    public void loadBanner(XBanner xBanner, Object o, View view, int position) {
        Glide.with(this).load(imgesUrl.get(position)).into((ImageView) view);
    }

    public void clickGallery(View view) {
        Intent intent = new Intent(MainActivity.this, GalleryListActivity.class);
        startActivity(intent);
    }

    public void clickCapture(View view) {
        Intent intent = new Intent(MainActivity.this, DgCamActivity.class);
        intent.putExtra(DgCamActivity.SELECT_IMG_URL,"");
        startActivity(intent);
    }

    public void clickIcons(View view) {
        Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
        intent.putExtra(WebViewActivity.PARAM_URL, ICONS_URL);
        intent.putExtra(WebViewActivity.PARAM_WEBVIEW_TITLE, getString(R.string.icon_folder_title));
        startActivity(intent);
    }

    public void clickSetting(View view) {
        Intent intent = new Intent(MainActivity.this, WebViewActivity.class);
        intent.putExtra(WebViewActivity.PARAM_URL, SETTINGS_URL);
        intent.putExtra(WebViewActivity.PARAM_WEBVIEW_TITLE, getString(R.string.setting_string));

        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        new MaterialDialog.Builder(this)
                .title(R.string.main_exist_confirm_title)
                .titleColor(getResources().getColor(R.color.black))
                .inputType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT)
                .content(R.string.main_exist_confirm_content)
                .contentColor(getResources().getColor(R.color.black))
                .negativeText(R.string.text_cancel)
                .positiveColorRes(R.color.dialog_positive_button_color)
                .positiveText(R.string.text_sure)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                    }
                }).show();
    }
}
