package com.special.camerafun;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by iristai on 2017/10/19.
 */

public class BottomSheet_ItemAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private ArrayList<Integer> intarray = new ArrayList<Integer>();
    private ClickListner mClickListner;

    public interface ClickListner {
        void onClick(int drawable);
    }

    public BottomSheet_ItemAdapter(Context context, ArrayList<Integer> imagesarray, ClickListner clickListner) {

        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
        this.intarray = imagesarray;
        this.mClickListner = clickListner;
    }

    @Override
    public int getCount() {
        return intarray.size();
    }

    @Override
    public Object getItem(int i) {
        return intarray.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        View v = mLayoutInflater.inflate(R.layout.bottom_sheet_item, viewGroup, false);

        ImageView imgView = (ImageView) v.findViewById(R.id.image);
//        imgView.setImageResource(intarray.get(position));
        Glide.with(mContext)
                .load(intarray.get(position))
                .into(imgView);
        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListner.onClick(intarray.get(position));
            }
        });

        return v;
    }
}
