package com.special.camerafun.webview;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.special.camerafun.DgCamActivity;
import com.special.camerafun.R;

/**
 * Created by iristai on 2017/11/5.
 */

public class WebViewActivity extends AppCompatActivity{

    private WebView mWebview;
    private String mUrl,mTitle;
    public static final String PARAM_URL = "webview_param_url";
    public static final String PARAM_WEBVIEW_TITLE = "webview_param_title";
    private ImageView mBackIcon;
    private TextView mTitleTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_layout);
        mUrl = getIntent().getExtras().getString(PARAM_URL);
        mTitle = getIntent().getExtras().getString(PARAM_WEBVIEW_TITLE);

        mTitleTextView = (TextView) findViewById(R.id.webview_title_text);
        mBackIcon = (ImageView) findViewById(R.id.webview_toolbar_img);
        mWebview = (WebView) findViewById(R.id.webview);

        mTitleTextView.setText(mTitle);
        WebSettings webSettings = mWebview.getSettings();
        webSettings.setJavaScriptEnabled(true);

        mWebview.addJavascriptInterface(new JavaScriptInterface(this), "JSInterface");

        mWebview.setWebViewClient(webViewClient);
        mWebview.loadUrl(mUrl);

//        setContentView(mWebview);
    }

    public class JavaScriptInterface {
        private Activity activity;
        public JavaScriptInterface(Activity activiy) {
            this.activity = activiy;
        }
            @JavascriptInterface
            public void showImg(String img_url){
                Intent intent = new Intent(WebViewActivity.this, DgCamActivity.class);
                intent.putExtra(DgCamActivity.SELECT_IMG_URL, img_url);
                startActivity(intent);
                WebViewActivity.this.finish();
        }
    }

    private WebViewClient webViewClient = new WebViewClient(){
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.d("IRIS","onPageFinished=" + url);
            if (mWebview.canGoBack()) {
                mBackIcon.setBackgroundResource(R.mipmap.ic_back);
            } else {
                mBackIcon.setBackgroundResource(R.mipmap.ic_close_white_48dp);
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            final String url;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                url = request.getUrl().toString();
                Log.d("IRIS","onPageStarted=" + url);
            }

            return super.shouldOverrideUrlLoading(view, request);

        }
    };
    public void clickWebViewIcon(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {


        if (mWebview.canGoBack()) {
            mWebview.goBack();

        } else {
            this.finish();
        }
    }
}
